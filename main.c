#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>

#include "ttf.h"
#include "music.h"
#include "bullet.h"
#include "enemy.h"

//
#define RATE 120

//window configuration constants
#define WINDOWTITLE "PlaneFire"
#define WINDOWWIDTH 1800
#define WINDOWHEIGHT 1500

//escape window
#define ESCAPEWIDTH (WINDOWWIDTH-200)
#define ESCAPEHEIGHT (WINDOWHEIGHT-200)

//font
#define FONTSIEZE 50

//texture
#define TEXTURENUMBER 1

//player's macros
#define PLANEFILEPATH "plane.png"
#define BULLETFILEPATH "bullet.png"
#define PLAYERBULLETSTEP 10
#define PLAYERBULLETSPEED 50
#define PLAYERBULLETWIDTH 32
#define PLAYERBULLETHEIGHT 32

//enemies
#define ENEMY_NUMBER 10
#define ENEMY_OCCURE_DELAY 1000
#define ENEMY_SPEED 1

SDL_Texture *texture_loaded[TEXTURENUMBER];
int texture_loaded_index = 0;

//window pointers
static SDL_Window *window = NULL;
static SDL_Renderer *window_renderer = NULL;

//player's resources
static SDL_Texture *player_plane = NULL;
static PlaneFire_Bul_Bullet player_bullet;
static int mouse_x = 0, mouse_y = 0;

//enemies
Enemy enemies[ENEMY_NUMBER];

//font resources
static PlaneFire_TTF_Font pixel_font = {
  NULL, NULL, NULL
};

//music's pointers
static PlaneFire_Mus_Music music;

static void draw_background(){

  SDL_SetRenderDrawColor(window_renderer, 135, 206, 250, 0xFF);//r g b a littleblue
  SDL_RenderClear(window_renderer);

}

static void draw_player_plane(int x, int y){

  int w, h;
  SDL_Rect r;
  SDL_QueryTexture(player_plane, NULL, NULL, &w, &h);

  r.w = w;
  r.h = h;

  if(x == -1 && y == -1){
    r.x = WINDOWWIDTH/2-w/2;
    r.y = WINDOWHEIGHT-h;
  }else{
    r.x = x-w/2;
    r.y = y-h/2;
  }

  SDL_RenderCopy(window_renderer, player_plane, NULL, &r);

}

static void event_keydown_process(SDL_Event *e, SDL_KeyCode *processing_key, SDL_bool *has_escape_key_processing){

  switch (e->key.keysym.sym) {
  case SDLK_ESCAPE:
    if(!(*has_escape_key_processing)){
      *processing_key = SDLK_ESCAPE;
      *has_escape_key_processing = SDL_TRUE;
    }
    else{
      *has_escape_key_processing = SDL_FALSE;
    }
    break;
  case SDLK_m:
    PlaneFire_Mus_ToggleMusic();
    break;
  }

}

static void draw_escape_window(){
  SDL_SetRenderDrawColor(window_renderer, 255, 182, 193, 128);//r g b a littlepink
  SDL_SetRenderDrawBlendMode(window_renderer, SDL_BLENDMODE_BLEND);

  SDL_Rect r = {(WINDOWWIDTH-ESCAPEWIDTH)/2, (WINDOWHEIGHT-ESCAPEHEIGHT)/2, ESCAPEWIDTH, ESCAPEHEIGHT};

  //set viewport
  SDL_RenderSetViewport(window_renderer, &r);

  SDL_RenderFillRect(window_renderer, NULL);

  SDL_Color text_color = {72, 61, 139, 255};//r g b a darkslateblue
  SDL_Rect text_rect = {0, 0, 0, 0};
  PlaneFire_TTF_RenderText(&pixel_font, window_renderer, "made by e0dZTBk", text_color, &text_rect);

  text_rect.y += 45;
  PlaneFire_TTF_RenderText(&pixel_font, window_renderer, "have fun!", text_color, &text_rect);

  text_rect.y += 45;
  PlaneFire_TTF_RenderText(&pixel_font, window_renderer, "press 'm' to toggle music", text_color, &text_rect);

  SDL_SetRenderDrawBlendMode(window_renderer, SDL_BLENDMODE_NONE);
  SDL_RenderSetViewport(window_renderer, NULL);//cancel viewport
}

static void event_key_draw(SDL_KeyCode processing_key, SDL_bool has_escape_key_processing){
  if(!has_escape_key_processing) return;

  switch (processing_key) {
  case SDLK_ESCAPE:
    draw_escape_window();
    break;
  default:
    break;
  }

}

static void draw_player_bullet(SDL_Point plane_point,  int step){
  plane_point.x -= 10;
  plane_point.y -= 40;
  PlaneFire_Bul_Render(&player_bullet, window_renderer, step, plane_point);
}

static void event_process(SDL_bool *is_quit){
  SDL_Event event;
  static SDL_bool has_player_moved = SDL_FALSE;
  static SDL_KeyCode processing_key;
  static SDL_bool has_escape_key_processing = SDL_FALSE;
  static SDL_Point plane_point= {WINDOWWIDTH/2, WINDOWHEIGHT};

  while(SDL_PollEvent(&event) != 0){
    switch (event.type) {
    case SDL_QUIT:
      *is_quit = SDL_TRUE;
      goto event_process_quit;
      break;
    case SDL_MOUSEMOTION:
	has_player_moved = SDL_TRUE;
	
	//
	mouse_x = event.motion.x;
	mouse_y = event.motion.y;
	//
	
	plane_point.x = mouse_x;
	plane_point.y = mouse_y;
      break;
    case SDL_KEYDOWN:
      event_keydown_process(&event, &processing_key, &has_escape_key_processing);
      break;
    }
  }

    if(!has_player_moved)
      draw_player_plane(-1, -1); //-1, -1 means the beginning position
    else
      draw_player_plane(mouse_x, mouse_y);

    draw_player_bullet(plane_point, PLAYERBULLETSTEP);

    event_key_draw(processing_key, has_escape_key_processing);

 event_process_quit:
  return;
}

static void rate_control_begin(int *ticks){
  *ticks = SDL_GetTicks();
}

static void rate_control_end(int *ticks){
  int cost = *ticks - SDL_GetTicks();

  if(cost <= 1000 / RATE){
    SDL_Delay(1000/RATE - cost);
  }else{
    printf("rate controlling failed!\n");
  }

}

static void process_enemies() {
  static int delay=0;
  static int nth=0;
  if (delay <= ENEMY_OCCURE_DELAY) {
    delay++;
  } else {
    if (nth < ENEMY_NUMBER) {
      Enemy_setstatus(&enemies[nth], SDL_TRUE);
      nth++;
      delay = 0;
    } else {
      //all enemies have been initialized
    }
  }

  if (nth > 0) {
    for (int i = 0; i<nth; ++i) {
      if(enemies[i].status==SDL_TRUE){
	SDL_Point newpos;
	SDL_Point oldpos = {enemies[i].cur_pos.x, enemies[i].cur_pos.y};
	int step_x = mouse_x > oldpos.x ? ENEMY_SPEED : (-1 * ENEMY_SPEED);
	int step_y = mouse_y > oldpos.y ? ENEMY_SPEED : (-1 * ENEMY_SPEED);
	newpos.x = oldpos.x + step_x;
	newpos.y = oldpos.y + step_y;
	Enemy_setpos(&enemies[i], newpos);
	if (newpos.y >= mouse_y) {
	  enemies[i].flip=SDL_FLIP_NONE;
	} else {
	  enemies[i].flip=SDL_FLIP_VERTICAL;
	}
      } else {
	//如果enemy已经死亡，就不更新位置
	;
      }
    }
  }
}

static void draw_enemies() {
  for (int i = 0; i < ENEMY_NUMBER; ++i) {
    Enemy_render(window_renderer, &enemies[i]);
  }
}

static int event_loop(){
  int flag = 0;
  SDL_bool is_quit = SDL_FALSE;
  int ticks;

  // the core of the loop
  while(!is_quit){

    rate_control_begin(&ticks);

    draw_background();

    process_enemies();
    
    draw_enemies();

    //update and draw(shit design)
    event_process(&is_quit);

    SDL_RenderPresent(window_renderer);

    rate_control_end(&ticks);

  }

 event_loop_quit:
  return flag;
}

static SDL_Texture *load_texture(const char *path){
  SDL_Texture *tmp = IMG_LoadTexture(window_renderer ,path);
  if(tmp != NULL)
    texture_loaded[texture_loaded_index++] = tmp;
  return tmp;
}

static int load_textures(){
  int flag = 0;

  player_plane = load_texture(PLANEFILEPATH);
  if(player_plane == NULL){
    SDL_Log("loading %s texture failed, %s\n", PLANEFILEPATH, IMG_GetError());
    flag++;
  }

 load_textures_quit:
  return flag;
}

static int load_fonts(){

  if(!PlaneFire_TTF_OpenFont(&pixel_font, "./ark-pixel-12px-monospaced-zh_cn.ttf", FONTSIEZE)){
    return 1;
  }
  return 0;
}

static int load_musics(){
  if(!PlaneFire_Mus_Load(&music, "./background_music.mp3", 0)){
    SDL_Log("failed to loading music!\n");
    return 1;
  }

  PlaneFire_Mus_PlayMusic(&music, -1);

  return 0;
}

static int load_bullets(){
  int flag = 0;

  if(!PlaneFire_Bul_Load(&player_bullet, window_renderer, BULLETFILEPATH, PLAYERBULLETWIDTH, PLAYERBULLETHEIGHT, PLAYERBULLETSPEED)){
    SDL_Log("loading bullet failed\n");
    flag++;
  }

  return flag;
}

static int load_enemies(){
  int flag = 0;

  Enemy_load(window_renderer, "enemy.png", enemies, ENEMY_NUMBER, window);
  
  return flag;
}


static int load_media(){
  int flag = 0;

  flag += load_textures();

  flag += load_fonts();

  flag+= load_musics();

  flag+= load_bullets();

  flag+=load_enemies();

 load_media_quit:
  return flag;
}

static int load(){
  int flag = 0;

  window =  SDL_CreateWindow(WINDOWTITLE, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WINDOWWIDTH, WINDOWHEIGHT, SDL_WINDOW_SHOWN);
  if(window == NULL){
    SDL_Log("creating window failed, %s\n", SDL_GetError());
    flag++;
    goto load_quit;
  }

  window_renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
  if(window_renderer == NULL){
    SDL_Log("creating window renderer failed, %s\n", SDL_GetError());
    flag++;
    goto load_quit;
  }

  flag += load_media();

 load_quit:
  return flag;
}

static void destroy_textures(SDL_Texture *texture_loaded[], int num){

  for (int i = 0; i < num; ++i) {
    SDL_DestroyTexture(texture_loaded[i]);
  }

}

static void close_fonts(){

  PlaneFire_TTF_CloseFont(&pixel_font);

}

static void close_musics(){
  if(music.is_loaded){
    PlaneFire_Mus_Free(&music);
  }
}

static void destroy_bullets(){
  PlaneFire_Bul_Free(&player_bullet);
}

static void destroy_enemies() {
  Enemy_free(enemies[0].texture);
}

static int close(){
  int flag = 0;

  destroy_textures(texture_loaded, TEXTURENUMBER);

  destroy_bullets();

  destroy_enemies();
  
  close_fonts();

  close_musics();

  SDL_DestroyRenderer(window_renderer);

  SDL_DestroyWindow(window);

  window = NULL;
  window_renderer = NULL;

  SDL_Quit();
  IMG_Quit();
  TTF_Quit();
  Mix_Quit();

 close_quit:
  return flag;
}

static int init(){
  int flag = 0;

  if(SDL_Init(SDL_INIT_VIDEO) != 0){
    SDL_Log("video initialization failed, %s\n", SDL_GetError());
    flag++;
    goto init_quit;
  }

  if(SDL_Init(SDL_INIT_AUDIO) != 0){
    SDL_Log("audio initialization failed, %s\n", SDL_GetError());
    flag++;
    goto init_quit;
  }

  int img_init_flag = IMG_INIT_PNG;
  if((!(IMG_Init(img_init_flag)) & img_init_flag) != 0){
    //IMG_Init return the flag's value if it successed
    //so if it doesn't equal to the flag's value, it failed
    SDL_Log("img initialization failed, %s\n", IMG_GetError());
    flag++;
    goto init_quit;
  }

  if(TTF_Init() == -1){
    SDL_Log("ttf initialization failed, %s\n", TTF_GetError());
    flag++;
    goto init_quit;
  }

  if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0){
    SDL_Log("mixer initialization failer, %s\n", Mix_GetError());
    flag++;
    goto init_quit;
  }

  if(SDL_ShowCursor(SDL_DISABLE) < 0){
    SDL_Log("hiding cursor failed, %s\n", SDL_GetError());
    flag++;
    goto init_quit;
  }

 init_quit:
  return flag;
}

int main(int argc, char *argv[])
{
  int flag = 0;
  int flag_last = flag;

  flag_last = flag;
  flag += init();
  if(flag != flag_last) goto main_quit;

  flag_last = flag;
  flag += load();
  if (flag != flag_last) goto main_quit;

  flag += event_loop();

 main_quit:
  flag += close();
  return flag;
}
