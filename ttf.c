#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "ttf.h"

SDL_bool PlaneFire_TTF_OpenFont(PlaneFire_TTF_Font *font, const char *path, int ptsize){

  font->font = TTF_OpenFont(path, ptsize);
  if(font->font == NULL){
    SDL_Log("failed to load %s font, %s\n", path, TTF_GetError());
    return SDL_FALSE;
  }

  font->surface = NULL;
  font->texture = NULL;
  
  return SDL_TRUE;
}

SDL_bool PlaneFire_TTF_RenderText(PlaneFire_TTF_Font *font, SDL_Renderer *renderer, const char *text, SDL_Color textcolor, SDL_Rect *r){

  if(font->surface != NULL)
    SDL_FreeSurface(font->surface);
  
  font->surface = TTF_RenderText_Solid(font->font, text, textcolor);
  if (font->surface == NULL) {
    SDL_Log("creating font surface fa, %s\n", TTF_GetError());
    return SDL_FALSE;
  }

  if(font->texture != NULL)
    SDL_DestroyTexture(font->texture);
  
  font->texture = SDL_CreateTextureFromSurface(renderer, font->surface);

  SDL_QueryTexture(font->texture, NULL, NULL, &(r->w), &(r->h));
  
  SDL_RenderCopy(renderer, font->texture, NULL, r);
  
}

void PlaneFire_TTF_CloseFont(PlaneFire_TTF_Font *font){

  if(font->surface != NULL)
    SDL_FreeSurface(font->surface);

  if (font->texture != NULL)
    SDL_DestroyTexture(font->texture);
  
  TTF_CloseFont(font->font);
}
