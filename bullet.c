#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "bullet.h"

SDL_bool PlaneFire_Bul_Load
(PlaneFire_Bul_Bullet *bullet,
 SDL_Renderer *window_renderer,
 char *path,
 int w,
 int h,
 int shoot_speed)
{
  bullet->texture = IMG_LoadTexture(window_renderer, path);
  if(bullet->texture == NULL){
    SDL_Log("failed to load bullet texture, %s\n", IMG_GetError());
    return SDL_FALSE;
  }

  bullet->bullet_index = bullet->shooting_bullet_num = 0;
  bullet->w = w;
  bullet->h = h;
  bullet->shoot_speed = shoot_speed;
  
  return SDL_TRUE;
}

void PlaneFire_Bul_Render
(PlaneFire_Bul_Bullet *bullet,
 SDL_Renderer *window_renderer,
 int step,
 SDL_Point shooting_point)
{

  static int delay_count = 0;
  
  SDL_Rect r = {0, 0, bullet->w, bullet->h};

  if(delay_count != 4000/bullet->shoot_speed){
    delay_count++;
  }else{
    delay_count = 0;
    //prepare the new shooted bullet
    if(bullet->shooting_bullet_num < BULLETMIXNUM){
      (bullet->shooting_bullet_num)++;
      bullet->bullet_index = bullet->shooting_bullet_num - 1;
    }else{
      if(bullet->bullet_index + 1 == BULLETMIXNUM){
	bullet->bullet_index = 0;
      }else{
	bullet->bullet_index++;
      }
    }
    bullet->bullet_points[bullet->bullet_index].x = shooting_point.x;
    bullet->bullet_points[bullet->bullet_index].y = shooting_point.y;
    bullet->step[bullet->bullet_index] = step;
  }
  
  //render the whole bullets using for loop
  for (int i = bullet->shooting_bullet_num - 1; i >= 0; --i) {
    bullet->bullet_points[i].y -= bullet->step[i];    
    r.x = bullet->bullet_points[i].x;
    r.y = bullet->bullet_points[i].y;    
    SDL_RenderCopy(window_renderer, bullet->texture, NULL, &r);
  }
}

SDL_Point Bullet_getpos(PlaneFire_Bul_Bullet *bullet, int index) {
  SDL_Point pos;
  pos.x=bullet->bullet_points[index].x;
  pos.y=bullet->bullet_points[index].y;
  return pos;
}

void PlaneFire_Bul_Free(PlaneFire_Bul_Bullet *bullet){

  SDL_DestroyTexture(bullet->texture);
  
}
