#ifndef PLANEFIRE_TTF_H
#define PLANEFIRE_TTF_H

#include <SDL2/SDL_ttf.h>
#include <SDL2/SDL.h>

typedef struct PlaneFire_TTF_Font
{
  TTF_Font *font;
  SDL_Surface *surface;
  SDL_Texture *texture;
  
} PlaneFire_TTF_Font;

SDL_bool PlaneFire_TTF_OpenFont(PlaneFire_TTF_Font *font, const char *path, int ptsize);

SDL_bool PlaneFire_TTF_RenderText(PlaneFire_TTF_Font *font, SDL_Renderer *renderer, const char *text, SDL_Color textcolor, SDL_Rect *r);

void PlaneFire_TTF_CloseFont(PlaneFire_TTF_Font *font);

#endif
