#ifndef PLANEFIRE_MUSIC_H
#define PLANEFIRE_MUSIC_H

#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#define CHUNKMIXNUM 5

typedef struct PlaneFire_Mus_Music {
  
  Mix_Music *music;
  Mix_Chunk *chunks[CHUNKMIXNUM];
  int loaded_chunks;
  SDL_bool is_loaded;
  
} PlaneFire_Mus_Music;

SDL_bool PlaneFire_Mus_Load(PlaneFire_Mus_Music *music, char *music_path, int num, ...);

void PlaneFire_Mus_PlayMusic(PlaneFire_Mus_Music *music, int repeat_times);

void PlaneFire_Mus_HaltMusic();

void PlaneFire_Mus_ToggleMusic();

void PlaneFire_Mus_PlaySoundEffect(PlaneFire_Mus_Music *music, int chunk_index, int repeat_times);

void PlaneFire_Mus_Free(PlaneFire_Mus_Music *music);

#endif /* PLANEFIRE_MUSIC_H */
