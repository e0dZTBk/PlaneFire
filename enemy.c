#include <SDL2/SDL_image.h>
#include <SDL2/SDL.h>
#include <stdlib.h>
#include <time.h>
#include "enemy.h"

void Enemy_load(SDL_Renderer *renderer, char *path, Enemy Enemya[],int num,SDL_Window *window) {
  SDL_Point window_size;
  SDL_GetWindowSize(window, &window_size.x, &window_size.y);
  int random_height_max=window_size.y/4;
  int random_width_max=window_size.x-30;

  srand(time(NULL));
  
  if(num<1)return;
  for (int i = 0; i < num; ++i) {
    if (i == 0) {
      Enemya[i].texture=IMG_LoadTexture(renderer, path);
      Enemya[i].n=i;
      int x=rand()%random_width_max;
      int y=rand()%random_height_max;
      Enemya[i].cur_pos.x=x;
      Enemya[i].cur_pos.y=y;
      Enemya[i].status=SDL_FALSE;
    } else {
      Enemya[i].texture=Enemya[0].texture;
      Enemya[i].n=i;
      int x=rand()%random_width_max;
      int y=rand()%random_height_max;
      Enemya[i].cur_pos.x=x;
      Enemya[i].cur_pos.y=y;
      Enemya[i].status=SDL_FALSE;
    }
  }
}

void Enemy_setpos(Enemy *enemy, SDL_Point pos) {
  enemy->cur_pos=pos;
}

void Enemy_setstatus(Enemy *enemy, SDL_bool cur_status) {
  enemy->status=cur_status;
}

void Enemy_render(SDL_Renderer *renderer, Enemy *enemy) {
  if (enemy->status == SDL_TRUE) {
    SDL_Rect dr;
    dr.x = enemy->cur_pos.x;
    dr.y = enemy->cur_pos.y;
    SDL_QueryTexture(enemy->texture, NULL, NULL, &dr.w, &dr.h);
    SDL_RenderCopyEx(renderer, enemy->texture, NULL, &dr, 0.0, NULL,
                     enemy->flip);
  }
}

void Enemy_free(SDL_Texture *texture) {
  SDL_DestroyTexture(texture);
}
