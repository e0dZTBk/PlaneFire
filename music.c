#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <stdarg.h>
#include "music.h"

SDL_bool PlaneFire_Mus_Load
(PlaneFire_Mus_Music *music, char *music_path, int num, ...){

  if(music_path != NULL){
    music->music = Mix_LoadMUS(music_path);
    if(music->music == NULL){
      SDL_Log("loading music %s failed, %s\n", music_path, Mix_GetError());
      return SDL_FALSE;
    }
  }else{
    music->music = NULL;
  }
  
  if(num == 0) goto PlaneFire_Mus_Load_quit;

  va_list ap;
  va_start(ap, num);

  for (int i = 0; i < CHUNKMIXNUM; ++i) {
    if(i >= num){
      music->chunks[i] = NULL;
    }else {
      music->chunks[i] = Mix_LoadWAV(va_arg(ap, char *));
      if(music->chunks[i] == NULL){
	SDL_Log("loading chunk %d failed, %s\n", i, Mix_GetError());
	return SDL_FALSE;
      }
    }
  }

  va_end(ap);

 PlaneFire_Mus_Load_quit:

  music->loaded_chunks = num;
  music->is_loaded = SDL_TRUE;
  return SDL_TRUE;
  
}

void PlaneFire_Mus_PlayMusic(PlaneFire_Mus_Music *music, int repeat_times){
  if(!music->is_loaded){
    SDL_Log("failed to play music, music hasn't been loaded!\n");
    return;
  }

  if(music->music != NULL){
    Mix_PlayMusic(music->music, repeat_times);
    return;
  }else{
    SDL_Log("failed to play music, music's pointer is NULL\n");
    return;
  }
}

void PlaneFire_Mus_HaltMusic(){
  if(Mix_PlayingMusic() == 0){
    SDL_Log("halt music error, no music is being played!\n");
    return;
  }else{
    Mix_HaltMusic();
  }
}

void PlaneFire_Mus_ToggleMusic(){
  if(Mix_PlayingMusic() == 0){
    SDL_Log("halt music error, no music is being played!\n");
    return;
  }else{
    if(Mix_PausedMusic() == 1){
      Mix_ResumeMusic();
    }else{
      Mix_PauseMusic();
    }
  }  
}

void PlaneFire_Mus_PlaySoundEffect
(PlaneFire_Mus_Music *music, int chunk_index, int repeat_times){
  if(!music->is_loaded){
    SDL_Log("failed to play sound effect, music hasn't been loaded!\n");
    return;
  }
  
  if(chunk_index <= (music->loaded_chunks - 1) && chunk_index >= 0){
    Mix_PlayChannel(-1, music->chunks[chunk_index], repeat_times);
  }else{
    SDL_Log("failed to play sound effect, the specified chunk doesn't exist!\n");
    return;
  }
}

static void PlaneFire_Mus_Free_freechunks(PlaneFire_Mus_Music *music){
  if(!music->is_loaded){
    SDL_Log("failed to free chunks, music hasn't been loaded before!\n");
    return;
  }

for (int i = 0; i < music->loaded_chunks; ++i) {
  Mix_FreeChunk(music->chunks[i]); 
 }
 
}

void PlaneFire_Mus_Free(PlaneFire_Mus_Music *music){
  if(music->music != NULL){
    Mix_FreeMusic(music->music);
  }

  PlaneFire_Mus_Free_freechunks(music);
  
}
