#include <SDL2/SDL.h>

typedef struct Enemy
{
  SDL_Texture *texture;
  int n;
  SDL_Point cur_pos;
  SDL_bool status;
  SDL_RendererFlip flip;
} Enemy;

void Enemy_load(SDL_Renderer *renderer,char *path, Enemy Enemya[],int num,SDL_Window *window);

void Enemy_setpos(Enemy *enemy,SDL_Point pos);

void Enemy_setstatus(Enemy *enemy,SDL_bool cur_status);

void Enemy_render(SDL_Renderer *renderer,Enemy *enemy);

void Enemy_free(SDL_Texture *texture);
