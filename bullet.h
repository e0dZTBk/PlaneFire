#ifndef PLANEFIRE_BULLET_H
#define PLANEFIRE_BULLET_H

#include <SDL2/SDL_image.h>
#include <SDL2/SDL.h>

#define BULLETMIXNUM 50

typedef struct PlaneFire_Bul_Bullet
{

  SDL_Texture *texture;
  int step[BULLETMIXNUM];
  SDL_Point bullet_points[BULLETMIXNUM];
  int w;
  int h;
  int bullet_index;
  int shooting_bullet_num;
  int shoot_speed;
  
} PlaneFire_Bul_Bullet;

SDL_bool PlaneFire_Bul_Load
(PlaneFire_Bul_Bullet *bullet,
 SDL_Renderer *window_renderer,
 char *path,
 int w,
 int h,
 int shoot_speed);

void PlaneFire_Bul_Render(PlaneFire_Bul_Bullet *bullet,
			  SDL_Renderer *window_renderer,
			  int step,
			  SDL_Point shooting_point);

SDL_Point Bullet_getpos(PlaneFire_Bul_Bullet *bullet,int index);

void PlaneFire_Bul_Free(PlaneFire_Bul_Bullet *bullet);

#endif /* PLANEFIRE_BULLET_H */
